package com.epam.model;

public class SyncClass implements Runnable{

    private static Object obj = new Object();
    private static Object obj1 = new Object();
    private static Object obj2 = new Object();

    void startClass() {
        Thread t1 = new Thread(this);
        Thread t2 = new Thread(this);
        Thread t3 = new Thread(this);
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("End");
    }

    private void method1() {
        synchronized (obj) {
            System.out.println("In method1.");
        }
    }

    private void method2() {
        synchronized (obj1) {
            System.out.println("In method2.");
        }
    }

    private void method3() {
        synchronized (obj2) {
            System.out.println("In method3.");
        }
    }

    @Override
    public void run() {
        this.method1();
        this.method2();
        this.method3();
        System.out.println("finish" + Thread.currentThread().getName());
    }
}
