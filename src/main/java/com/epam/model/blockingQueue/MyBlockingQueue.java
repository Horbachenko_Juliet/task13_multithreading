package com.epam.model.blockingQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MyBlockingQueue implements Runnable{

    BlockingQueue<Integer> queue = new ArrayBlockingQueue<>(5);

    public MyBlockingQueue() {
        Thread thread1 = new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(500);
                    queue.put(i);
                    System.out.println("Put ->" + i);
                } catch (InterruptedException e) {e.printStackTrace();}
            }
        },"Thread 1");

        Thread thread2 = new Thread(() -> {
            Integer value;
            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(3000);
                    value = queue.take();
                    System.out.println("Take ->" + value);
                } catch (InterruptedException e) {e.printStackTrace();}
            }
        },"Thread 2");
        thread1.start();
        thread2.start();
    }

    @Override
    public void run() {

    }
}
