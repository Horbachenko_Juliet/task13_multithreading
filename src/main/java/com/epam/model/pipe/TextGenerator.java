package com.epam.model.pipe;

import java.io.IOException;
import java.io.Writer;

public class TextGenerator extends Thread{
    private Writer out;

    public TextGenerator(Writer out) {
        this.out = out;
    }

    public void run () {
        try {
            try{
                for (char c = 'A'; c <= 'Z'; c++)
                    out.write(c + " ");
            } finally {
                out.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
