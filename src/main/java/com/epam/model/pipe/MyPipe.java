package com.epam.model.pipe;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;

public class MyPipe {

    public MyPipe() {
        PipedWriter out = new PipedWriter();
        PipedReader in = null;
        try {
            in = new PipedReader(out);
            TextGenerator textGenerator = new TextGenerator(out);
            textGenerator.start();
            int ch;
            while ((ch = in.read()) != -1)
                System.out.print((char) ch);
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
