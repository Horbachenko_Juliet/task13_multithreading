package com.epam.model.fibonacciSequence;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FibinacciSequenceCallable {


    ExecutorService executorService = Executors.newSingleThreadExecutor();
    public Future<Integer> future = executorService.submit(() ->{
        int i = 10;
        int sum = 0;
        int b = 0;
        int c = 1;
        System.out.print("Fibonacci series: ");
        for (int j = 1; j <= i; j++) {
            int a = b;
            b = c;
            c = a + b;
            System.out.print(a + " ");
            sum += a;
        }
        System.out.println();
        return sum;
    });
}
