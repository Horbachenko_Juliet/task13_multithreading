package com.epam.model.fibonacciSequence;

public class FibonacciSequence implements Runnable {

    private final Thread t;
    private int i;
    private int b = 0;
    private int c = 1;

    public FibonacciSequence(int i) {
        this.i = i;
        t = new Thread(this);
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void run() {
        System.out.print("Fibonacci series: ");
        for (int j = 1; j <= i; j++) {
            int a = b;
            b = c;
            c = a + b;
            System.out.print(a + " ");
        }
        System.out.println();
    }
}
