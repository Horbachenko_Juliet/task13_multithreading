package com.epam.model;


public interface Model {

    void startPingPong();

    void createFibonacci();

    void createFibonacciWithExecutors();

    void createFibonacciWithCallable();

    void createSleeper(int i);

    void createSynchronizedMethods();

    void createCommunicatedPipes();

    void createReadWriteLock();

}
