package com.epam.view;


import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new ControllerImpl();
        setMenu();
        this.methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
    }

    private void setMenu(){
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - start ping - pong program...");
        menu.put("2", " 2 - produce a Fibonacci numbers sequence...");
        menu.put("3", " 3 - produce Fibonacci sequence with executors...");
        menu.put("4", " 4 - produce Fibonacci sequence with Callable...");
        menu.put("5", " 5 - create a sleeper program...");
        menu.put("6", " 6 - create three methods with synchronize...");
        menu.put("7", " 7 - create two communicated pipes...");
        menu.put("8", " 8 - create ReadWriteLock...");
        menu.put("Q", " Q - Quit");
    }

    private void pressButton1() {
        controller.startPingPong();
    }

    private void pressButton2() { controller.createFibonacci(); }

    private void pressButton3() {
        controller.createFibonacciWithExecutors();
    }

    private void pressButton4() { controller.createFibonacciWithCallable(); }

    private void pressButton5() {
        System.out.println("Enter number of threads...");
        int i = Integer.parseInt(input.nextLine());
        controller.createSleeper(i); }

    private void pressButton6() { controller.createSynchronizedMethods(); }

    private void pressButton7() { controller.createCommunicatedPipes(); }

    private void pressButton8() { controller.createReadWriteLock(); }


    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
