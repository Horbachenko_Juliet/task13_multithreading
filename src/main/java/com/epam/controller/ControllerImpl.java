package com.epam.controller;


import com.epam.model.Model;
import com.epam.model.ModelImpl;

public class ControllerImpl implements Controller {

    private Model model;

    public ControllerImpl() {
        model = new ModelImpl();
    }

    @Override
    public void startPingPong() {
        model.startPingPong();
    }

    @Override
    public void createFibonacci() {
        model.createFibonacci();
    }

    @Override
    public void createFibonacciWithExecutors() {
        model.createFibonacciWithExecutors();
    }

    @Override
    public void createFibonacciWithCallable() {
        model.createFibonacciWithCallable();
    }

    @Override
    public void createSleeper(int i) {
        model.createSleeper(i);
    }

    @Override
    public void createSynchronizedMethods() {
        model.createSynchronizedMethods();
    }

    @Override
    public void createCommunicatedPipes() {
        model.createCommunicatedPipes();
    }

    @Override
    public void createReadWriteLock() {
        model.createReadWriteLock();
    }
}