package com.epam.controller;

public interface Controller {

    void startPingPong();

    void createFibonacci();

    void createFibonacciWithExecutors();

    void createFibonacciWithCallable();

    void createSleeper(int i);

    void createSynchronizedMethods();

    void createCommunicatedPipes();

    void createReadWriteLock();
}



